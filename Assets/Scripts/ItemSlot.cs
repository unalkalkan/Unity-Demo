﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour {

	public Item _item;
	public Item item {
		get{
			return _item;
		}
		set{
			if(value == null)
				_item = null;
			else
				OnItemChange(value);
			
			UpdateUI();
		}
	}
	
	[HideInInspector] public Image itemImageComponent;
	private Image backgroundImageComponent;
	public SlotType slotType;
    public Sprite emptySlotSprite;
	private Button button;
	public Inspect inspect;

	void Awake(){
		backgroundImageComponent = transform.Find("BackgroundImage").GetComponent<Image>();
		itemImageComponent = transform.Find("ItemImage").GetComponent<Image>();
		button = GetComponent<Button>();

		button.onClick.AddListener(OnClick);
	}
	
	// OnItemChange(Item newItem):
	// Defined rules of accepted item types for each different slot type.
	// Also indicates what is going to be done when an item dropped into the itemslot
	
	private void OnItemChange(Item newItem){
		switch(slotType){
			case SlotType.Backpack:{
				_item = newItem;
				newItem.gameObject.transform.parent = transform;
				break;
			}
			case SlotType.Disenchant:{
				if(newItem.disenchantItem != null){
					GameObject disenchantInstance = Instantiate(newItem.disenchantItem.gameObject, transform.position, transform.rotation);
					disenchantInstance.name = newItem.disenchantItem.name;
					InventoryManager.instance.AddItem(disenchantInstance.GetComponent<Item>());
				}
				Destroy(newItem.gameObject);
				break;
			}
			case SlotType.Upgrade:{
				if(newItem.stats.ContainsKey(StatType.Upgradeable)){
					DropHandler.CancelDrop = true;
					_item = newItem;
					newItem.gameObject.transform.parent = transform;

					foreach(KeyValuePair<StatType, StatModifier> stat in _item.stats){
						if(stat.Key == StatType.Upgradeable){
							stat.Value.Value += 1f;
						}else{
							stat.Value.Value += 10f;
						}
					}
					DragHandler.draggedItemSlot.item = null;
					Debug.Log("Upgradeable completed.");
				}else{
					DropHandler.CancelDrop = true;
					Debug.Log("Not upgradeable");
				}
				break;
			}
			case SlotType.HeadArmor:{
				if(newItem.itemType == ItemType.HeadArmor){
					_item = newItem;
					newItem.gameObject.transform.parent = transform;
					
					// Equip Item and Display stats
					_item.Equip(InventoryManager.instance.character);
				}else{
					// Not head armor
					DropHandler.CancelDrop = true;
				}
				break;
			}
			case SlotType.ChestArmor:{
				if(newItem.itemType == ItemType.ChestArmor){
					_item = newItem;
					newItem.gameObject.transform.parent = transform;
					
					// Equip Item and Display stats
					_item.Equip(InventoryManager.instance.character);
				}else{
					// Not chest armor
					DropHandler.CancelDrop = true;
				}
				break;
			}
			case SlotType.LegArmor:{
				if(newItem.itemType == ItemType.LegArmor){
					_item = newItem;
					newItem.gameObject.transform.parent = transform;

					// Equip Item and Display stats
					_item.Equip(InventoryManager.instance.character);
				}else{
					// Not leg armor
					DropHandler.CancelDrop = true;
				}
				break;
			}
			case SlotType.Weapon:{
				if(newItem.itemType == ItemType.Weapon){
					_item = newItem;
					newItem.gameObject.transform.parent = transform;

					// Equip Item and Display stats
					_item.Equip(InventoryManager.instance.character);
				}else{
					// Not a weapon
					DropHandler.CancelDrop = true;
				}
				break;
			}
			default:{
				// Wrong slot somehow
				DropHandler.CancelDrop = true;
				break;
			}
		}
	}

	private void UpdateUI(){
		if( item != null ){
			backgroundImageComponent.sprite = item.activeSlotSprite;
			itemImageComponent.sprite = item.itemSprite;
			itemImageComponent.enabled = true;
		}else{
			backgroundImageComponent.sprite = emptySlotSprite;
			itemImageComponent.sprite = null;
			itemImageComponent.enabled = false;
		}
	}

	// Inspect settings
	private void OnClick(){
		if(_item != null){
			inspect.ShowInspect(this);
		}else{
			if(Inspect.instance != null)
				Destroy(Inspect.instance.gameObject);
		}
	}
}
