﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStats : MonoBehaviour {

	public static DisplayStats instance;

    public Transform StatsParant;
    public GameObject StatContent;
	public Character character;

	private Dictionary<Text, Text> display = new Dictionary<Text, Text>();
	private Text statName;

	void Awake(){
		if( instance != null)
			return;
		instance = this;
	}
	void Start () {
		foreach(CharacterStat stat in character.CharacterStats){
			GameObject tempObject = Instantiate(StatContent, transform.position, transform.rotation);
			tempObject.transform.SetParent(StatsParant);

			Text statName = tempObject.transform.Find("StatName").GetComponent<Text>();
			statName.text = stat.StatType.ToString();
			Text statValue = tempObject.transform.Find("StatValue").GetComponent<Text>();
			statValue.text = stat.BaseValue.ToString();
			display.Add(statName, statValue);
		}
	}
	
	public void UpdateStats () {
		foreach(CharacterStat stat in character.CharacterStats)
			foreach(KeyValuePair<Text,Text> line in display){
				if(line.Key.text == stat.StatType.ToString()){
					line.Value.text = stat.Value.ToString();
				}
			}
	}
}
