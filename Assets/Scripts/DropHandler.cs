﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour, IDropHandler {
	private ItemSlot itemSlot;
	public static bool CancelDrop = false;

	void Awake(){
		itemSlot = GetComponent<ItemSlot>();		
	}

	public void OnDrop(PointerEventData eventData){
		// Change previous slot's raycast information
		DragHandler.draggedItemSlot.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;

		if(DragHandler.draggedItemSlot.item != null){
			if(DragHandler.draggedItemSlot.item.equipped){
				DragHandler.draggedItemSlot.item.UnEquip(InventoryManager.instance.character);
			}
			if(itemSlot.item == null ){
				// If target slot is empty
				itemSlot.item = DragHandler.draggedItemSlot.item;
				if(CancelDrop){
					CancelDrop = false;
					return;
				}
				DragHandler.draggedItemSlot.item = null;
			}else{
				// If target slot have an item
				SwapItems();
			}
		}
			
			
			// if(itemSlot.item.equipped == true){
			// 	Debug.Log("Equipped bool changed to unequipped");
			// 	DragHandler.draggedItemSlot.item.UnEquip(InventoryManager.instance.character);
			// }
	}

	private void SwapItems(){
		GameObject swapObject = Instantiate(itemSlot.gameObject, itemSlot.gameObject.transform.position, itemSlot.gameObject.transform.rotation);
		swapObject.transform.SetParent(transform.parent);
		swapObject.SetActive(false);

		ItemSlot temp = swapObject.GetComponent<ItemSlot>();
		temp.item = itemSlot.item;

		itemSlot.item = DragHandler.draggedItemSlot.item;
		if(CancelDrop){
			CancelDrop = false;
			return;
		}
		DragHandler.draggedItemSlot.item = temp.item;
		
		Destroy(swapObject);
	}
}