﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour{
	public string itemName = "Item";
	public ItemType itemType;
	public Sprite itemSprite = null;
    public Sprite activeSlotSprite = null;
	public Item disenchantItem;
	public Dictionary<StatType, StatModifier> stats = new Dictionary<StatType, StatModifier>();

	public bool equipped;

	public void Equip(Character c){
		foreach(KeyValuePair<StatType, StatModifier> stat in stats){
			// Hold list in Character class so that comparison would be quite easy.
			switch(stat.Key){
				case StatType.Damage:{
					c.Damage.AddModifier(stat.Value);
					break;
				}
				case StatType.Defense:{
					c.Defense.AddModifier(stat.Value);
					break;
				}
				case StatType.Health:{
					c.Health.AddModifier(stat.Value);
					break;
				}
				case StatType.Mana:{
					c.Mana.AddModifier(stat.Value);
					break;
				}
				case StatType.Vitality:{
					c.Vitality.AddModifier(stat.Value);
					break;
				}
			}
		}
		equipped = true;
		DisplayStats.instance.UpdateStats();
	}

	public void UnEquip(Character c){
		foreach(KeyValuePair<StatType, StatModifier> stat in stats){
			switch(stat.Key){
				case StatType.Damage:{
					c.Damage.RemoveAllModifiersFromSource(this);
					break;
				}
				case StatType.Defense:{
					c.Defense.RemoveAllModifiersFromSource(this);
					break;
				}
				case StatType.Health:{
					c.Health.RemoveAllModifiersFromSource(this);
					break;
				}
				case StatType.Mana:{
					c.Mana.RemoveAllModifiersFromSource(this);
					break;
				}
				case StatType.Vitality:{
					c.Vitality.RemoveAllModifiersFromSource(this);
					break;
				}
			}
		}
		equipped = false;
		DisplayStats.instance.UpdateStats ();
	}
}