
public enum SlotType{
	Backpack,
	Disenchant,
	HeadArmor,
	ChestArmor,
	LegArmor,
	Weapon,
	Upgrade
}