﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler{
	public static ItemSlot draggedItemSlot;

	private ItemSlot itemSlot;
	private Vector3 startPosition;
	private Transform startParent;
	private CanvasGroup canvasGroup;

	void Awake(){
		itemSlot = transform.parent.GetComponent<ItemSlot>();
		canvasGroup = transform.parent.GetComponent<CanvasGroup>();
	}

	public void OnBeginDrag(PointerEventData eventData){
		if(Inspect.instance != null)
			Inspect.instance.HideInspect();
		startPosition = transform.position;
		startParent = transform.parent;
		draggedItemSlot = itemSlot;
		canvasGroup.blocksRaycasts = false;
	}
	public void OnDrag(PointerEventData data){
		transform.position = Input.mousePosition;
	}
	public void OnEndDrag(PointerEventData eventData){
		draggedItemSlot = null;

		canvasGroup.blocksRaycasts = true;
		if (transform.parent == startParent){
			transform.position = startPosition;
		}
	}
}
