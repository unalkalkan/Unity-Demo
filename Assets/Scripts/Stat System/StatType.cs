
public enum StatType{
    Health,
    Mana,
    Damage,
    Defense,
    Vitality,
    Upgradeable,
    Disenchantable
}