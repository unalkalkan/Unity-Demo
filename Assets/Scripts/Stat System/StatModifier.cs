
public class StatModifier{
    public float Value;
    public readonly StatModType Type;
    public readonly Item Source;

    public StatModifier(float value, StatModType type, Item source){
        Value = value;
        Type = type;
        Source = source;
    }
    public StatModifier(float value, StatModType type) : this(value, type, null){ }
}