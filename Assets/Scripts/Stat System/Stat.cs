using System;
using System.Collections.Generic;

public class Stat{
    public StatType StatType;
    public float BaseValue;
    private readonly List<StatModifier> statModifiers;

    public float Value{
        get{
            return CalculateFinalValue();
        }
    }
    public Stat(StatType statType, float baseValue){
        StatType = statType;
        BaseValue = baseValue;
        statModifiers = new List<StatModifier>();
    }
    public void AddModifier(StatModifier mod){
        statModifiers.Add(mod);
    }
    public bool RemoveModifier(StatModifier mod){
        return statModifiers.Remove(mod);
    }

    private float CalculateFinalValue(){
        float finalValue = BaseValue;
        for(int i = 0; i < statModifiers.Count; i++){
            StatModifier mod = statModifiers[i];
            switch(mod.Type){
                case StatModType.Flat:{
                    finalValue += mod.Value;
                    break;
                }
                case StatModType.Percent:{
                    finalValue *= 1 + mod.Value;
                    break;
                }
            }
        }
        return (float)Math.Round(finalValue, 4);
    }
    public void RemoveAllModifiersFromSource(Item source){
        statModifiers.Clear();
    }
}