using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour{
    public List<CharacterStat> CharacterStats;

    public CharacterStat Health;
    public CharacterStat Mana;
    public CharacterStat Damage;
    public CharacterStat Defense;
    public CharacterStat Vitality;

    void Awake(){
        Health = new CharacterStat(StatType.Health, 150f);
        Mana = new CharacterStat(StatType.Mana, 100f);
        Damage = new CharacterStat(StatType.Damage, 55f);
        Defense = new CharacterStat(StatType.Defense, 75f);
        Vitality = new CharacterStat(StatType.Vitality, 150f);

        CharacterStats = new List<CharacterStat>();
        CharacterStats.Add(Health);
        CharacterStats.Add(Mana);
        CharacterStats.Add(Damage);
        CharacterStats.Add(Defense);
        CharacterStats.Add(Vitality);
    }
}