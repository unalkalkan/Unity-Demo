﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

	public static InventoryManager instance;
	public Character character;
	
	[Header("Backpack Slots")]
	public Transform backpackSlotsParent;
	public Sprite backpackEmptySlotSprite;
	public SlotType backpackSlotType;
	private ItemSlot[] backpackSlots;

	[System.Serializable]
	public class EquipmentSlot{
		public ItemSlot itemSlot;
		public Sprite emptySlotSprite;
		public SlotType slotType;
	}
	[Space(10)]
	public EquipmentSlot[] equipmentSlots;

	[Space(10)]
	[Header("Disenchant Materials")]
	public GameObject[] disenchantItems;

	void Awake(){
		if (instance != null){
			return;
		}
		instance = this;

		backpackSlots = backpackSlotsParent.GetComponentsInChildren<ItemSlot>();
		foreach(ItemSlot current in backpackSlots){
			current.slotType = backpackSlotType;
			current.emptySlotSprite = backpackEmptySlotSprite;
		}
		foreach(EquipmentSlot current in equipmentSlots){
			current.itemSlot.slotType = current.slotType;
			current.itemSlot.emptySlotSprite = current.emptySlotSprite;
		}
	}
	
	public bool AddItem(Item newItem){
		foreach(ItemSlot slot in backpackSlots){
			if(slot.item == null){
				slot.item = newItem;
				newItem.gameObject.transform.parent = slot.transform;
				return true;
			}
		}
		// Inventory is full
		Debug.Log("I'm overburdened.");
		Destroy(newItem.transform);
		return false;
	}
}