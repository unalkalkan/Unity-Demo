﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inspect : MonoBehaviour {
	
	public static Inspect instance;
	
	public Text ItemName;
	public Text ItemType;
	public Transform StatsParent;
	public GameObject ItemStat;
	

	void Awake(){
		if(instance != null){
			return;
		}
		instance = this;
	}

	public void ShowInspect(ItemSlot itemSlot){
		if(instance != null){
			Destroy(instance.gameObject);
		}
		GameObject inspectObject = Instantiate(gameObject, itemSlot.transform.position, itemSlot.transform.rotation);
		inspectObject.transform.SetParent(GameObject.Find("Inventory").transform);
		instance = inspectObject.GetComponent<Inspect>();

		instance.transform.position = itemSlot.transform.position;
		ShowData(itemSlot.item);
	}

	public void HideInspect(){
		Destroy(instance.gameObject);
	}

	private void ShowData(Item item){
		instance.ItemName.text = item.itemName;
		instance.ItemType.text = item.itemType.ToString();

		foreach(KeyValuePair<StatType, StatModifier> stat in item.stats){
			GameObject temp = Instantiate(ItemStat, instance.transform.position, instance.transform.rotation);
			temp.transform.SetParent(instance.StatsParent);
			Text line = temp.GetComponent<Text>();
			line.text = "<color=#FFFFFF>" + stat.Key.ToString() + "</color> : <color=#90b3ed>"+stat.Value.Value+"</color>";
			if(stat.Value.Type == StatModType.Percent){
				line.text += " %";
			}
		}
	}
}
