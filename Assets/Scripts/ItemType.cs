
public enum ItemType{
	Ingredient,
	HeadArmor,
	LegArmor,
	ChestArmor,
	Weapon
}