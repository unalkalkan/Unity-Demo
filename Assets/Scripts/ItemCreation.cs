﻿using System;
using System.IO;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCreation : MonoBehaviour {
	public InputField itemName;
	public Dropdown itemType;
	public Dropdown disenchantItem;
	public GameObject statHolderPrefab;
	public Button AddStatButton;
	public Transform statsParent;
	
	public Button button;
	public GameObject itemPrefab;

	public StatHolder[] holders;
	private List<string> statTypes;
	private int sayac = 0;

	void Start () {
		InitializePropertiesFields();
		InitializeStatFields();	
		//button.onClick.AddListener(CreateItem);
	}

	void InitializePropertiesFields(){
		List<string> enumNames = new List<string>(Enum.GetNames(typeof(ItemType)));
		itemType.AddOptions(enumNames);
		List<string> disenchantItemNames = new List<string>();

		foreach(GameObject item in InventoryManager.instance.disenchantItems){
			disenchantItemNames.Add(item.GetComponent<Item>().itemName);
		}

		disenchantItem.AddOptions(disenchantItemNames);
	}

	void InitializeStatFields(){
		statTypes = new List<string>(Enum.GetNames(typeof(StatType)));
		//AddStatButton.onClick.AddListener(InstantiateStatHolder);
	}

	public void InstantiateStatHolder(){
		CheckPreviousStat();
		GameObject temp = Instantiate(statHolderPrefab, transform.position, transform.rotation);
		temp.name = "Holder "+sayac;
		sayac++;
		temp.transform.SetParent(statsParent);
		Dropdown d = temp.GetComponentInChildren<Dropdown>();
		d.AddOptions(statTypes);
	}

	private void CheckPreviousStat(){
		StatHolder[] holders =  statsParent.GetComponentsInChildren<StatHolder>();
		StatHolder previousHolder;
		if(holders.Length > 0){
			previousHolder = holders[holders.Length - 1];
			previousHolder.dropdown.interactable = false;
			string statTypeName = previousHolder.dropdown.options[previousHolder.dropdown.value].text;
			if(statTypes.Contains(statTypeName)){
				statTypes.Remove(statTypeName);
			}
		}
		if(holders.Length == 4){
			AddStatButton.interactable = false;
		}
	}

	private List<string> GetSpriteFiles(string path){
		List<string> spritePaths = new List<string>();

		#if UNITY_ANDRIOD
		string defaultPath = Application.streamingAssetsPath;
		DirectoryInfo dir = new DirectoryInfo(Path.Combine(defaultPath , path));
		FileInfo[] info = dir.GetFiles("*.png");
		#else
		DirectoryInfo dir = new DirectoryInfo(Path.Combine(Application.dataPath , path));
		FileInfo[] info = dir.GetFiles("*.png");
		#endif
		foreach (FileInfo f in info) {
			spritePaths.Add(f.Name);
		}
		return spritePaths;
	}

	private Sprite GetSprite(ItemType itemType){
		string temp;
		switch(itemType){
			case ItemType.ChestArmor:{
				temp = "Sprites/Items/ChestArmor";
				return Resources.Load<Sprite>(temp);
			}case ItemType.HeadArmor:{
				temp = "Sprites/Items/HeadArmor";
				return Resources.Load<Sprite>(temp);
			}case ItemType.Ingredient:{
				temp = "Sprites/Items/HealthPotion";
				return Resources.Load<Sprite>(temp);
			}case ItemType.LegArmor:{
				temp = "Sprites/Items/LegArmor";
				return Resources.Load<Sprite>(temp);
			}
			case ItemType.Weapon:{
				temp = "Sprites/Items/Weapon";
				return Resources.Load<Sprite>(temp);
			} default:{
				temp = "Sprites/Items/Sand";
				return Resources.Load<Sprite>(temp);
			}
		}
	}

	public void CreateItem(){
		GameObject instance = Instantiate(itemPrefab, Vector3.zero, Quaternion.identity);
		instance.transform.parent = InventoryManager.instance.backpackSlotsParent;
		Item temp = instance.GetComponent<Item>();

		instance.name = temp.itemName = itemName.text;
		temp.itemType =  (ItemType)itemType.value;
		temp.disenchantItem = InventoryManager.instance.disenchantItems[disenchantItem.value].GetComponent<Item>();
		temp.itemSprite = GetSprite(temp.itemType);
		temp.activeSlotSprite = Resources.Load<Sprite>("Sprites/Inventory/ActiveSlot");

		// Adding Stats
		StatHolder[] holders =  statsParent.GetComponentsInChildren<StatHolder>();
		foreach(StatHolder holder in holders){
			string selectedString = holder.dropdown.options[holder.dropdown.value].text;
			StatType selectedStatType = (StatType)Enum.Parse(typeof(StatType), selectedString);
			float selectedValue = float.Parse(holder.inputField.text);
			temp.stats.Add(selectedStatType, new StatModifier(selectedValue, StatModType.Flat));
		}
		
		InventoryManager.instance.AddItem(temp);
	}
}
