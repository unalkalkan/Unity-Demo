﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateItems : MonoBehaviour {
	public GameObject itemPrefab;

	void Start(){
		Item Sand = InventoryManager.instance.disenchantItems[0].GetComponent<Item>();
		// Item Sand = CreateItem("Sand", ItemType.Ingredient, "Sand", "ActiveSlot", null);
		Item Boots = CreateItem("Boots", ItemType.LegArmor, "LegArmor", "DifferentSlot1", Sand);
		Boots.stats.Add(StatType.Health, new StatModifier(15f, StatModType.Flat));
		Boots.stats.Add(StatType.Mana, new StatModifier(2f, StatModType.Flat));
		Boots.stats.Add(StatType.Vitality, new StatModifier(5f, StatModType.Flat));

		Item ChestArmor = CreateItem("ChestArmor", ItemType.ChestArmor, "ChestArmor", "DifferentSlot1", Sand);
		ChestArmor.stats.Add(StatType.Defense, new StatModifier(15f, StatModType.Flat, ChestArmor));
		ChestArmor.stats.Add(StatType.Health, new StatModifier(25f, StatModType.Flat));
		ChestArmor.stats.Add(StatType.Vitality, new StatModifier(10f, StatModType.Flat));
		ChestArmor.stats.Add(StatType.Upgradeable, new StatModifier(1f, StatModType.Flat));

		Item Helmet = CreateItem("Helmet", ItemType.HeadArmor, "HeadArmor", "DifferentSlot1", Sand);
		Helmet.stats.Add(StatType.Defense, new StatModifier(5f, StatModType.Flat));
		Helmet.stats.Add(StatType.Vitality, new StatModifier(5f, StatModType.Flat));

		Item Weapon = CreateItem("Weapon", ItemType.Weapon, "Weapon", "DifferentSlot2", Sand);
		Weapon.stats.Add(StatType.Damage, new StatModifier(25f, StatModType.Flat, Weapon));
		// InventoryManager.instance.AddItem(Sand);
		InventoryManager.instance.AddItem(Boots);
		InventoryManager.instance.AddItem(ChestArmor);
		InventoryManager.instance.AddItem(Helmet);
		InventoryManager.instance.AddItem(Weapon);
	}

	Item CreateItem(string itemName, ItemType itemType, string itemSpriteName, string activeSlotSpriteName, Item disenchantItem){
		GameObject instance = Instantiate(itemPrefab, Vector3.zero, Quaternion.identity);
		instance.transform.parent = InventoryManager.instance.backpackSlotsParent;
		Item temp = instance.GetComponent<Item>();

		instance.name = temp.itemName = itemName;
		temp.itemType = itemType;
		temp.disenchantItem = disenchantItem;
		temp.itemSprite = Resources.Load<Sprite>("Sprites/Items/" + itemSpriteName);
		temp.activeSlotSprite = Resources.Load<Sprite>("Sprites/Inventory/" + activeSlotSpriteName);
		return temp;
	}
}
